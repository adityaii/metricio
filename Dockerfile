FROM ubuntu:latest

ADD . /app

WORKDIR /app

RUN apt-get update -y && apt-get install -y nodejs npm && apt-get upgrade nodejs npm -y && apt-get autoremove -y

RUN npm install 

EXPOSE 3000

CMD npm run start
